const sum = require("./sum");

//Utilitarios que são executados antes e depois dos testes

//Chamado antes e depois de cada teste
beforeEach(() => {
  //Digamos que varios testes interagem com um banco de dados, é aqui que a conexão
  //é inicializada
  console.log("CODIGO AQUI RODA ANTES DE CADA TESTE");
});

afterEach(() => {
  //e aqui é onde a conexão é fechada
  //Podendo ser substituido pelo metodo afterAll() que roda apos todos os testes serem finalizados
  console.log("CODIGO AQUI RODA DEPOIS DE CADA TESTE");
});

//Chamado uma vez apenas, uma antes e uma depois de todos os testes serem finalizados
beforeAll(() => {
  console.log("CODIGO AQUI RODA ANTES DE TODOS OS TESTES");
});

afterAll(() => {
  console.log("CODIGO AQUI RODA APOS TODOS OS TESTES FINALIZAREM");
});

describe("Testing Sum Function", () => {
  it("Adds 1 + 2 to equal 3", () => {
    expect(sum(1, 2)).toBe(3);
  });
});

test("Atribuição de Objeto", () => {
  const obj = { one: 1 };
  obj["two"] = 2;

  expect(obj).toEqual({ one: 1, two: 2 });
});

test("Adicionando numeros positivos não é zero", () => {
  for (let a = 1; a < 10; a++) {
    for (let b = 1; b < 10; b++) {
      expect(a + b).not.toBe(0);
    }
  }
});

test("nulo", () => {
  const n = null;

  expect(n).toBeNull(); //Nulo
  expect(n).toBeDefined(); //Definido
  expect(n).not.toBeUndefined(); //O contrário de Indefinido
  expect(n).not.toBeTruthy(); //O contrário de Verdadeiro
  expect(n).toBeFalsy(); //Falso
});

test("Zero", () => {
  const z = 0;

  expect(z).not.toBeNull(); //Nulo
  expect(z).toBeDefined(); //Definido
  expect(z).not.toBeUndefined(); //Contrário de Indefinido
  expect(z).not.toBeTruthy(); //Contrário de Verdadeiro
  expect(z).toBeFalsy(); //Falso
});

test("Dois + Dois", () => {
  const value = 2 + 2;

  expect(value).toBeGreaterThan(3); //O resultado é Maior que 3
  expect(value).toBeGreaterThanOrEqual(3.5); //O resultado é Maior ou Iqual a 3.5
  expect(value).toBeLessThan(5); //O resultado é Menor que 5
  expect(value).toBeLessThanOrEqual(4.5); //O resultado é Menor que 4.5

  //toBe e toEqual são equivalentes para números
  expect(value).toBe(4); //O resultado é 4
  expect(value).toEqual(4); //O resultado é 4
});

test("Adicionando números de ponto flutuante", () => {
  const value = 0.1 + 0.2;

  //Para igualdade de pontos flutuantes usar o toBeCloseTo
  //expect(value).toBe(0.3); //Não funciona por causo da arredondamento 0.30000000000000004
  expect(value).toBeCloseTo(0.3);
});

//Verificando String em expressões regulares
test("Não existe I em team", () => {
  expect("team").not.toMatch(/I/);
});

test("Existe stop no nome Christoph", () => {
  expect("Christoph").toMatch(/stop/);
});

//Verificando se existe um item dentro do array / iterable
const shoppingList = [
  "diapers",
  "kleenex",
  "trash bags",
  "paper towels",
  "beer"
];

test("Encontrando item baseado nome", () => {
  expect(shoppingList).toContain("beer");
  expect(new Set(shoppingList)).toContain("beer");
});

//Testando se uma função lança um erro quando é chamada
function compileAndroidCode() {
  throw new Error("you are using the wrong JDK");
}

test("Compiling android goes as expected", () => {
  expect(compileAndroidCode).toThrow();
  expect(compileAndroidCode).toThrow(Error);

  //Passando qual mensagem de error, ou a regex
  expect(compileAndroidCode).toThrow("you are using the wrong JDK");
  expect(compileAndroidCode).toThrow(/JDK/);
});

//Testando codigo assincrono
function fetchData(callback) {
  let timer = null;
  let message = null;

  timer = setTimeout(() => {
    clearTimeout(timer);
    message = "peanut butter";
    return callback(message);
  }, 500);
}

test("Buscando dados assincronos", done => {
  function callback(data) {
    try {
      expect(data).toBe("peanut butter");
      done();
    } catch (error) {
      done(error);
    }
  }

  fetchData(callback);
});

//Testando codigo assincrono usando Promise
const user = {
  name: "Lean",
  lastName: "Silva",
  city: "Brasília-DF"
};

function getUser() {
  let timer = null;

  return new Promise((resolve, reject) => {
    timer = setTimeout(() => {
      clearTimeout(timer);
      resolve(user);
    }, 500);
  });
}

function rejectUser() {
  let timer = null;

  return new Promise((resolve, reject) => {
    timer = setTimeout(() => {
      clearTimeout(timer);
      reject("error");
    }, 500);
  });
}

test("Buscando dados assincronos [Promise]", async () => {
  const verifyUser = await getUser().then(user => {
    expect(user).toEqual({ ...user });
  });

  return verifyUser;
});

//Usando o metchers .resolves, .rejects
test("retorna o objeto user", () => {
  return expect(getUser()).resolves.toEqual({ ...user });
});

test("retorna um erro ao buscar por user", () => {
  return expect(rejectUser()).rejects.toMatch("error");
});

//Async | Await
test("user", async () => {
  await expect(getUser()).resolves.toEqual({ ...user });
});

test("reject [user]", async () => {
  await expect(rejectUser()).rejects.toMatch("error");
});

//////// Mock Functions ////////
